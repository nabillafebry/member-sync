# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY /member-sync.jar /member-sync.jar
# run application with this command line[
CMD ["java", "-jar", "/member-sync.jar"]
